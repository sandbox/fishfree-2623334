<?php

/**
 * Implements hook_views_data()
 **/
function similarusersbyterms_views_data_alter(&$data) {
  $data['similarusersbyterms']['similarusersbyterms']['moved to'] = array('users', 'similarusersbyterms');
  $data['users']['similarusersbyterms'] = array(
    'title' => t('Similarity'),
    'group' => t('Similar Users By Terms'),
    'help' => t('Percentage/count of terms which user has in common with the user given as argument.'),
    'field' => array(
      'handler' => 'similarusersbyterms_handler_field_similar',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'similarusersbyterms_handler_sort_similar',
    ),
  );
  $data['similarusersbyterms']['uid']['moved to'] = array('users', 'similar_uid');
  $data['users']['similar_uid'] = array(
    'title' => t('Uid'),
    'group' => t('Similar Users By Terms'),
    'help' => t('ID of user(s). Passes term ids to Similar Users By Terms.'), // The help that appears on the UI,
    'argument' => array(
      'handler' => 'similarusersbyterms_handler_argument_user_uid',
      // 'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'uid',
    ),
  );
}
