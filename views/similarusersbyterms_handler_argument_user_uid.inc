<?php
/**
 * Argument handler to accept a user id.
 *
 * @ingroup views_argument_handlers
 */
class similarusersbyterms_handler_argument_user_uid extends views_handler_argument_numeric {
  /**
   * Override the behavior of title(). Get the name of the user.
   */
  function title_query() {
    $titles = array();

    $select = db_select('users', 'u');
    $select->addField('u', 'name');
    $select->condition('u.uid', $this->value, 'IN');
    //$select->addTag('node_access');
    $result = $select->execute();

    foreach ($result as $term) {
      $titles[] = check_plain($term->name);
    }

    return $titles;
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['vocabularies'] = array('default' => array());
    $options['include_args'] = array('default' => FALSE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    unset($form['not']);

    $vocabularies = array();
    $result = db_query('SELECT vid, name FROM {taxonomy_vocabulary} ORDER BY weight');
    foreach ($result as $vocabulary) {
      $vocabularies[$vocabulary->vid] = $vocabulary->name;
    }

    $form['similarusersbyterms'] = array(
      '#type' => 'fieldset',
      '#title' => t('Similarity by Terms'),
      '#collapsible' => FALSE,
    );
    $form['similarusersbyterms']['vocabularies'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Limit similarity to terms within these vocabularies'),
      '#description' => t('Choosing any vocabularies here will limit the terms used to calculate similarity. It is usually best NOT to limit the terms, but in some cases this is necessary. Leave all checkboxes unselected to not limit terms.'),
      '#options' => $vocabularies,
      '#default_value' => empty($this->options['vocabularies']) ? array() : $this->options['vocabularies'],
    );

    $form['similarusersbyterms']['include_args'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include argument node(s) in results'),
      '#description' => t('If selected, the user(s) passed as the argument will be included in the view results.'),
      '#default_value' => !empty($this->options['include_args']),
    );

  }

  function validate_arg($arg) {
    // first run the inherited arg validation
    if (!parent::validate_arg($arg)) {
      return FALSE;
    }

    // hmmm... @todo: wildcard validation?
    // see views_handler_argument.inc for possible code
    if (!empty($this->options['break_phrase'])) {
      views_break_phrase($this->argument, $this);
    }
    else {
      $this->value = array($this->argument);
    }

    // $vocabulary_vids is array of vocabulary ids (a.k.a. vids, confusing right?)
    $vocabulary_vids = empty($this->options['vocabularies']) ? array() : $this->options['vocabularies'];
    foreach ($vocabulary_vids as $key => $val) {
      if ($val == 0) {
        unset($vocabulary_vids[$key]);
      }
    }

    //Because taxonomy_index database table created by Drupal core does not provide entity-integration. So we need dependent on the taxonomy_entity_index contrib module which creates the database table with the same name, and query the entity_id column as the user id .
    $select = db_select('taxonomy_entity_index', 'tei');
    $select->addField('tei', 'tid');

    if (count($vocabulary_vids)) {
      $select->join('taxonomy_term_data', 'td', 'tei.tid = td.tid');
      $select->condition('td.vid', $vocabulary_vids, 'IN');
    }
    $select->condition('tei.entity_id', $this->value, 'IN');
    $select->condition('tei.bundle', 'user');
    $result = $select->execute();

    $this->tids = array();
    foreach ($result as $row) {
      $this->tids[$row->tid] = $row->tid;
    }
    $this->view->tids = $this->tids;

    if (count($this->tids) == 0) {
      // there are no terms ... we need to cancel the query and bail out
      return FALSE;
    }

    return TRUE;
  }

  function query($group_by = FALSE) {
    $this->ensure_my_table();

    $this->query->add_table('taxonomy_entity_index', NULL, NULL, 'similarusersbyterms_taxonomy_entity_index');
    $this->query->add_where(0, "similarusersbyterms_taxonomy_entity_index.tid", $this->tids, 'IN');

    // exclude the current user(s)
    if (empty($this->options['include_args'])) {
      $this->query->add_where(0, "users.uid", $this->value, 'NOT IN');
    }
  }

}
