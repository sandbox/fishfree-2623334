## Similar Users By Terms ##
module for Drupal
created by fishfree
- - - - - - - - - - - - - - - - - -

### How to use: ###

1. You need to install and enable [Taxonomy Entity Index](https://www.drupal.org/project/taxonomy_entity_index) module, then access admin/config/system/taxonomy-entity-index to generate term entity index;
2. Then just refer to  README.markdown in the [Similar by terms](http://cgit.drupalcode.org/similarterms/tree/README.markdown) module, conceptually replace nodes in that module with users in this module.
